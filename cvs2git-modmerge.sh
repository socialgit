#!/bin/sh

rm -fr /tmp/cvs2git.tmp
cd $HOME
mkdir $2
cd $2
git --bare init --shared
CVSROOT=$1
GITLOCATION=`pwd`

for dir in `modules-in-cvs`; do
	mkdir /tmp/cvs2git.tmp.$dir
	cd /tmp/cvs2git.tmp.$dir
	git cvsimport -o master -v -d $CVSROOT $dir
	mkdir _tmp
	git add _tmp
	echo "creating tempdir to move sub-project $dir into." | git commit -a -F -
	git mv [A-Za-z0-9]* _tmp/
	git mv _tmp $dir
	echo "moving $dir into place for merger." | git commit -a -F -
	echo "pushing $dir..."
	git pull $GITLOCATION master
	git push $GITLOCATION master
	echo "pushed $dir."
done
